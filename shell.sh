#!/bin/bash

# 安装镜像
docker pull hyperledger/fabric-peer:2.2.0
docker pull hyperledger/fabric-ccenv:2.2.0
docker pull hyperledger/fabric-tools:2.2.0
docker pull hyperledger/fabric-orderer:2.2.0
docker pull hyperledger/fabric-ca:1.4.0

# 给镜像打上标签
docker tag hyperledger/fabric-peer:2.2.0 hyperledger/fabric-peer:latest
docker tag hyperledger/fabric-tools:2.2.0 hyperledger/fabric-tools:latest
docker tag hyperledger/fabric-orderer:2.2.0 hyperledger/fabric-orderer:latest
docker tag hyperledger/fabric-ccenv:2.2.0 hyperledger/fabric-ccenv:latest
docker tag hyperledger/fabric-ca:1.4.0 hyperledger/fabric-ca:latest
